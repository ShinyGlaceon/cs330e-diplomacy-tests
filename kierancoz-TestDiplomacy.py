from Diplomacy import diplomacy_solve

from io import StringIO
from unittest import main, TestCase

class TestDiplomacy(TestCase):

    def test_solve1(self):
        input = ["A ElPaso Hold"]
        output = diplomacy_solve(input)
        self.assertEqual(["A ElPaso"], output)
        print()

    def test_solve2(self):
        input = ["A ElPaso Hold", "B Orlando Move ElPaso"]
        output = diplomacy_solve(input)
        self.assertEqual(["A [dead]", "B [dead]"], output)
        print()

    def test_solve3(self):
        input = ["C Sydney Support A", "A ElPaso Hold", "B Orlando Move ElPaso", "D London Support B"]
        output = diplomacy_solve(input)
        self.assertEqual(["A [dead]", "B [dead]", "C Sydney", "D London"], output)
        print()
    
    def test_solve4(self):
        input = ["A ElPaso Hold", "B Orlando Move ElPaso", "C Sydney Support B", "D London Move Sydney"]
        output = diplomacy_solve(input)
        self.assertEqual(["A [dead]","B [dead]","C [dead]","D [dead]"], output)
        print()
        
    def test_solve5(self):
        input = ["A ElPaso Hold", "B Orlando Move ElPaso", "C Sydney Move Orlando", "D London Support A"]
        output = diplomacy_solve(input)
        self.assertEqual(["A ElPaso","B [dead]", "C Orlando","D London"], output)
    
    def test_solve6(self):
        input = ["A ElPaso Hold", "B Orlando Move ElPaso", "C Austin Support B"]
        output = diplomacy_solve(input)
        self.assertEqual(["A [dead]","B ElPaso","C Austin"], output)
    
    def test_solve7(self):
        input = ["A Portland Move Orlando","B Orlando Hold","C Austin Move Portland","D Detroit Support A"]
        output = diplomacy_solve(input)
        self.assertEqual(["A Orlando","B [dead]", "C Portland", "D Detroit"], output)

if __name__ == "__main__": #pragma: no cover
    main()
